﻿namespace MysqlIdentityInitial.config
{
    public interface IAuthResourceConfig
    {
        string audienceToken { get; set; }
        string issuerToken { get; set; }
        string secretKey { get; set; }
        //string urlBaseIssuerToken { get; set; }

        //string pathIssuerToken { get; set; }
    }
}