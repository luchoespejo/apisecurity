﻿using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Owin;

namespace MysqlIdentityInitial.config
{
    public static class AuthConfigManager
    {
        private static AuthConfig Get<T>()
        {
            var fileName = typeof(T).Name;
            var fullPath = HttpContext.Current.Server.MapPath($"~/config/{fileName}.json");
            var jsonText = File.ReadAllText(fullPath);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthConfig>(jsonText);
            return result;
        }

        public static IAuthResourceConfig GetConfigurationJwt(this IAppBuilder app)
        {
            return Get<AuthConfig>();
        }

        public static IAuthIssuerTokenConfig GetConfigurationJwt(this ApiController app)
        {
            return Get<AuthConfig>();
        }
        public static IAuthIssuerTokenConfig GetConfigurationJwt(this Controller app)
        {
            return Get<AuthConfig>();
        }


    }


    internal class AuthConfig : IAuthResourceConfig, IAuthIssuerTokenConfig
    {
        public string secretKey { get; set; }
        public string urlBaseIssuerToken { get; set; }
        public string pathIssuerToken { get; set; }
        public string issuerToken { get; set; }
        public string audienceToken { get; set; }
    }
}