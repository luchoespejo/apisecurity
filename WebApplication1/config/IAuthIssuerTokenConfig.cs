﻿namespace MysqlIdentityInitial.config
{
    /// <summary>
    /// data about token generator
    /// </summary>
    public interface IAuthIssuerTokenConfig
    {
        string urlBaseIssuerToken { get; set; }
        string pathIssuerToken { get; set; }
    }
}