﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using MysqlIdentityInitial.config;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class SecurityController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public SecurityController()
        {
        }

        public SecurityController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [System.Web.Mvc.AllowAnonymous]
        [System.Web.Mvc.Route("token")]
        [System.Web.Mvc.HttpPost]
        public async Task<HttpResponseMessage> GenerateExternalToken(LoginForInputViewModel inputData)
        {
            var cfg = this.GetConfigurationJwt();
            var clientHttp = new HttpClient { BaseAddress = new Uri(cfg.urlBaseIssuerToken) };
            clientHttp.DefaultRequestHeaders.Accept.Clear();
            clientHttp.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await clientHttp.PostAsJsonAsync(cfg.pathIssuerToken, inputData);
            response.EnsureSuccessStatusCode();
            
            var result = await SignInManager.PasswordSignInAsync(inputData.email, inputData.password, true, shouldLockout: false);
           

            return response;


        }
    }
}
