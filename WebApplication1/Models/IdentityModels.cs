﻿
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MySql.AspNet.Identity;

namespace WebApplication1.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public int DatabaseConnectionId { get; set; }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            
            // Agregar aquí notificaciones personalizadas de usuario
            userIdentity.AddClaim(new Claim("DatabaseConnectionId", this.DatabaseConnectionId.ToString()));

            return userIdentity;
        }
    }
    public static class IdentityExtensions
    {
        public static int GetDatabaseConnectionId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("DatabaseConnectionId");
            var id = int.Parse(claim.Value);
            return id;
        }
    }

    //public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    //{
    //    public ApplicationDbContext()
    //        : base("DefaultConnection", throwIfV1Schema: false)
    //    {
    //    }

    //    public static ApplicationDbContext Create()
    //    {
    //        return new ApplicationDbContext();
    //    }
    //}
}