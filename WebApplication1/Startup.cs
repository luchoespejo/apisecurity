﻿using System;
using System.IdentityModel.Tokens;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using MysqlIdentityInitial.config;
using Owin;
using TecnologiasSynagro.mobile.Providers;


[assembly: OwinStartupAttribute(typeof(WebApplication1.Startup))]
namespace WebApplication1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            ConfigureOAuth(app, app.GetConfigurationJwt());

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);

            ConfigureAuth(app);
        }
        public void ConfigureOAuth(IAppBuilder app, IAuthResourceConfig cfg)
        {
            //var issuer = "SynagroBackend";
            //var audience = "MobileApi";
            //var secretKey = "ksZsZnxJEdN9S0AxDj9x2u2XFX35gWYd";


            var issuer = cfg.issuerToken;
            var audience = cfg.audienceToken;
            var secretKey = cfg.secretKey;

            var secretKeyArray = Encoding.ASCII.GetBytes(secretKey);
            var base64EncodedString = Convert.ToBase64String(secretKeyArray);

            //var k1 = Encoding.ASCII.GetBytes(secretKey);
            //  Microsoft.IdentityModel.Tokens
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new InMemorySymmetricSecurityKey(secretKeyArray),

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = audience,

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero

            };
            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseOAuthBearerTokens(new OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new PathString("/Token"),
                AuthenticationType = OAuthDefaults.AuthenticationType,
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(60),
                Provider = new ApplicationOAuthProvider(),
                AllowInsecureHttp = true,

            });
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {

                    AuthenticationMode = AuthenticationMode.Active,

                    AllowedAudiences = new[] { audience },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                       // new AuthenticationTokenProvider()
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, base64EncodedString),

                    },

                    Provider = new OAuthBearerAuthenticationProvider()
                    {
                        OnValidateIdentity = context =>
                        {
                            context.Ticket.Identity.AddClaim(new System.Security.Claims.Claim("newCustomClaim", "newValue"));
                            //var identity = HttpContext.Current.GetOwinContext().Authentication.User.Identity;
                            return Task.FromResult<object>(null);
                        }
                    },
                    TokenValidationParameters = tokenValidationParameters

                });


            //OAuthOptions = new OAuthAuthorizationServerOptions
            //{
            //    //TokenEndpointPath = new PathString("/Token"),
            //    //Provider = new ApplicationOAuthProvider(PublicClientId),
            //    //AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
            //    //AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
            //    //// In production mode set AllowInsecureHttp = false
            //    //AllowInsecureHttp = true,
            //    Provider =  new SimpleAuthorizationServerProvider(),
            //};

            ////// Enable the application to use bearer tokens to authenticate users


        }
    }

}
